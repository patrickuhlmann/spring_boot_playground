package ch.uhlme.playground.validation;

import jakarta.validation.*;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static org.springframework.test.util.AssertionErrors.assertTrue;

public class BenchmarkTest {
    @Test
    void should_be_valid_small() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = factory.getValidator();

            // about 20 objects
            var city = createSmallCity();

            long startMillis = System.currentTimeMillis();
            validator.validate(city);
            System.out.println("First validation (small, java bean) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

            // about 20 objects
            city = createSmallCity();

            startMillis = System.currentTimeMillis();
            Set<ConstraintViolation<City>> violations = validator.validate(city);
            System.out.println("Second validation (small, java bean) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

            assertTrue("should have no validation errors", violations.isEmpty());
        }
    }

    @Test
    void should_be_valid_small_custom() {
        // about 20 objects
        var city = createSmallCity();

        long startMillis = System.currentTimeMillis();
        city.isValid();
        System.out.println("First validation (small, custom) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

        // about 20 objects
        city = createSmallCity();

        startMillis = System.currentTimeMillis();
        boolean isValid = city.isValid();
        System.out.println("Second validation (small, custom) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

        assertTrue("should have no validation errors", isValid);
    }

    @Test
    void should_be_valid_large() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = factory.getValidator();

            // about 100'000 objects
            var city = createLargeCity();

            long startMillis = System.currentTimeMillis();
            validator.validate(city);
            System.out.println("First validation (large, java bean) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

            // about 100'000 objects
            city = createLargeCity();

            startMillis = System.currentTimeMillis();
            Set<ConstraintViolation<City>> violations = validator.validate(city);
            System.out.println("Second validation (large, java bean) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

            assertTrue("should have no validation errors", violations.isEmpty());
        }
    }

    @Test
    void should_be_valid_large_custom() {
        // about 100'000 objects
        var city = createLargeCity();

        long startMillis = System.currentTimeMillis();
        city.isValid();
        System.out.println("First validation (large, custom) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

        // about 100'000 objects
        city = createLargeCity();

        startMillis = System.currentTimeMillis();
        boolean isValid = city.isValid();
        System.out.println("Second validation (large, custom) executed in " + (System.currentTimeMillis() - startMillis) + " ms");

        assertTrue("should have no validation errors", isValid);
    }

    private City createLargeCity() {
        return new City(IntStream.range(0, 1000).mapToObj(i -> createLargeBuilding()).toList());
    }

    private Building createLargeBuilding() {
        return new Building(IntStream.range(0, 100).mapToObj(i -> new Person("Person " + i)).toList());
    }

    private City createSmallCity() {
        return new City(List.of(
                createSmallBuilding(),
                createSmallBuilding(),
                createSmallBuilding()
        ));
    }

    private Building createSmallBuilding() {
        return new Building(List.of(
                new Person("Hans"),
                new Person("Friedrich"),
                new Person("Paul"),
                new Person("Susan"),
                new Person("Mary"),
                new Person("Simone")
        ));
    }
}
