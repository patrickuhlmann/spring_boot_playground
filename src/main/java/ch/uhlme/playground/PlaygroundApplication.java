package ch.uhlme.playground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
		"ch.uhlme.playground.config",
		"ch.uhlme.playground.helloworld"
})
public class PlaygroundApplication {
	public static void main(String[] args) {
		SpringApplication.run(PlaygroundApplication.class, args);
	}
}
