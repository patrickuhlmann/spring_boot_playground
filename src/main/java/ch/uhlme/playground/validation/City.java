package ch.uhlme.playground.validation;

import jakarta.validation.Valid;

import java.util.List;

public class City {
    @Valid
    private final List<Building> buildings;

    public City(List<Building> buildings) {
        this.buildings = buildings;
    }

    public boolean isValid() {
        return buildings.stream().allMatch(Building::isValid);
    }
}
