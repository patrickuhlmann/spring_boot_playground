package ch.uhlme.playground.validation;

import jakarta.validation.constraints.NotNull;

public class Person {
    @NotNull
    private final String name;

    public Person(String name) {
        this.name = name;
    }

    public boolean isValid() {
        return name != null;
    }
}
