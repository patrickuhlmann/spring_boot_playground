package ch.uhlme.playground.validation;

import jakarta.validation.Valid;

import java.util.List;

public class Building {
    @Valid
    private final List<Person> residents;

    public Building(List<Person> residents) {
        this.residents = residents;
    }

    public boolean isValid() {
        return residents.stream().allMatch(Person::isValid);
    }
}
