# spring_boot_playground

Playground for Spring Boot Experiments

Based on Java 21

## Run with Docker

```
./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=uhlme/springbootplayground
docker run -p 8080:8080 -t uhlme/springbootplayground
```
